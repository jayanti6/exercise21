function SelectElements(){

}

SelectElements.prototype.selectClass = function(selector){
  return $(selector);
}

SelectElements.prototype.findListItem = function(){
  //using 1 is the fastest selector because IDs are unique
  return $("#myListItem");                          //1
  console.log($("ul#myList").find("li").eq(2));     //2
  console.log($("li#listItem_2").next());           //3
}

SelectElements.prototype.findSearchInput = function(){
  return $("label[for='q']");
}

SelectElements.prototype.findNumberOfHiddenElements = function(){
  var hiddenElements = $("body").find(":hidden").not("script");
  var numberOfHiddenElements = hiddenElements.length;
  return numberOfHiddenElements;
}

SelectElements.prototype.findNumberofImagesWithAltAttr = function(){
  var numberOfImagesWithAltAttr = $("img[alt]").length;
  return numberOfImagesWithAltAttr;
}

SelectElements.prototype.selectOddRows = function(){
  return $("table tbody  tr").filter(":odd");
}

$(document).ready(function(){

  var selectElement = new SelectElements();
  // 1
  selectElement.selectClass("div.module");

  //2
  selectElement.findListItem();

  //3
  selectElement.findSearchInput();

  //4
  selectElement.findNumberOfHiddenElements();

  //5
  selectElement.findNumberofImagesWithAltAttr();

  //6
  selectElement.selectOddRows();
});
